#include "stdafx.h"
#include "SceneMgr.h"

SceneMgr::SceneMgr(int _iWidth, int _iHeight) :m_DeletaTime(0.0f), m_IdxX(0), m_IdxY(0)
{
	for (int i = 0; i < MAX_OBJECTS; ++i) { m_pObjects[i] = nullptr; }
	//Initialize() 호출 
	Initialize(_iWidth, _iHeight);
}

SceneMgr::~SceneMgr()
{
	m_pRenderer->DeleteTexture(m_TestTexture);
	m_pRenderer->DeleteTexture(m_BulletTexture);
	m_pRenderer->DeleteTexture(m_AniTexture);
	if (m_pRenderer) {
		delete m_pRenderer;
		m_pRenderer = nullptr;
	}
	for (int i = 0; i < MAX_OBJECTS; ++i) { DeleteObject(i); }
}

void SceneMgr::Initialize(int _iWidth, int _iHeight)
{
	// Initialize Renderer
	m_pRenderer = new Renderer(_iWidth, _iHeight);
	m_ScreenWidth = _iWidth;
	m_ScreenHeight = _iHeight;

	m_TestTexture = m_pRenderer->CreatePngTexture("./isaac.png");
	m_AniTexture = m_pRenderer->CreatePngTexture("./effect.png");
	m_BulletTexture = m_pRenderer->CreatePngTexture("./tears.png");
	m_EnemyTexture = m_pRenderer->CreatePngTexture("./Ghost.png");
	m_BackgroundTexture = m_pRenderer->CreatePngTexture("./BackGround.png");
	float fWidth = (float)m_ScreenWidth / 100.f;
	float fHeight = (float)m_ScreenHeight / 100.f;

	// Add Hero
	AddObject(0.0f, 0.f, 0.f, 0.f, 0.f, 0.0f, 0.8f, 0.8, 0.8f, KIND_HERO, 100.f, STATE_GROUND);
	// Add Boundary
	AddObject(fWidth * 0.5f - 0.3f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.5f, fHeight, 0.f, KIND_BUILDING, 100.f, STATE_GROUND);
	AddObject(-fWidth * 0.5f + 0.3f, 0.f, 0.f, 0.f, 0.f, 0.f, 1.f, fHeight, 0.f, KIND_BUILDING, 100.f, STATE_GROUND);
	AddObject(0.f, fHeight * 0.5f - 0.3f, 0.f, 0.f, 0.f, 0.f, fWidth, 1.f, 0.f, KIND_BUILDING, 100.f, STATE_GROUND);
	AddObject(0.f, -fHeight * 0.5f + 0.3f, 0.f, 0.f, 0.f, 0.f, fWidth, 1.f, 0.f, KIND_BUILDING, 100.f, STATE_GROUND);
	// Add Enemy
	AddObject(2.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.5f, 0.5f, 0.f, KIND_ENEMY, 5, STATE_GROUND);
}

void SceneMgr::DeleteObject(unsigned int _Idx)
{
	if (_Idx > MAX_OBJECTS) return;
	if (m_pObjects[_Idx]) {
		delete m_pObjects[_Idx];
		m_pObjects[_Idx] = nullptr;
	}
}

// 위치, 크기, 가속도, kind, hp, state로 변경해야 함
int SceneMgr::AddObject(float _fPosX, float _fPosY, float _fPosZ, float _fVelX, float _fVelY, float _fVelZ,
	float _fWidth, float _fHeight, float _fDepth, int _iKind, int _iHp, int _iState)
{
	int Idx = FindEmptySlot();
	if (Idx < 0) return -1;
	m_pObjects[Idx] = new Object();
	m_pObjects[Idx]->SetPos(Vec3D(_fPosX, _fPosY, _fPosZ));
	m_pObjects[Idx]->SetVel(Vec3D(_fVelX, _fVelY, _fVelZ));
	m_pObjects[Idx]->SetSize(Vec3D(_fWidth, _fHeight, _fDepth));
	m_pObjects[Idx]->SetColor(Color(1.f, 1.f, 1.f, 1.f));
	m_pObjects[Idx]->SetMass(1.f);
	m_pObjects[Idx]->SetFrictionCoef(0.05f);
	m_pObjects[Idx]->SetKind(_iKind);
	m_pObjects[Idx]->SetHp(_iHp);
	m_pObjects[Idx]->SetState(_iState);
	return Idx;
}

int SceneMgr::FindEmptySlot()
{
	int Idx = -1;
	for (int i = 0; i < MAX_OBJECTS; ++i) {
		if (m_pObjects[i] == nullptr) {
			Idx = i;
			break;
		}
	}
	if (Idx == -1) std::cout << "All Slots are occupied." << std::endl;
	return Idx;
}

void SceneMgr::Update(float _fTime)
{
	m_pObjects[HERO_ID]->Update(_fTime);

	for (int i = 1; i < MAX_OBJECTS; ++i)
	{
		if (m_pObjects[i] != nullptr)
		{
			m_pObjects[i]->Update(_fTime);
		}
	}
	m_DeletaTime += _fTime;
	if (0.1f < m_DeletaTime)
	{
		++m_IdxX;
		if (5 == m_IdxX) {
			m_IdxX = 0;
			m_IdxY++;

			if (8 == m_IdxY) {
				m_IdxY = 0;
			}
		}
		m_DeletaTime = 0.0f;
	}
}

void SceneMgr::ApplyForce(Vec3D& force, float _fTime)
{
	// int state = 0;
	if (m_pObjects[HERO_ID]->GetState() == STATE_AIR) {
		force.z = 0.0f;
	}
	m_pObjects[HERO_ID]->ApplyForce(force, _fTime);
}

void SceneMgr::UpdateCollision()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_pObjects[i] == NULL) continue;
		int CollisionCounter = 0;
		for (int j = i + 1; j < MAX_OBJECTS; j++)
		{
			if (m_pObjects[j] == NULL) continue;
			if (i == j) { continue; }

			float minX = 0.f, minY = 0.f, maxX = 0.f, maxY = 0.f; //src
			float minX1 = 0.f, minY1 = 0.f, maxX1 = 0.f, maxY1 = 0.f; //dst
			Vec3D pos, size;
			pos = m_pObjects[i]->GetPos();
			size = m_pObjects[i]->GetSize();
			minX = pos.x - size.x * 0.5f;
			maxX = pos.x + size.x * 0.5f;
			minY = pos.y - size.y * 0.5f;
			maxY = pos.y + size.y * 0.5f;

			pos = m_pObjects[j]->GetPos();
			size = m_pObjects[j]->GetSize();

			minX1 = pos.x - size.x * 0.5f;
			maxX1 = pos.x + size.x * 0.5f;
			minY1 = pos.y - size.y * 0.5f;
			maxY1 = pos.y + size.y * 0.5f;
			if (RRCollision(minX, minY, maxX, maxY, minX1, minY1, maxX1, maxY1)) {
				CollisionCounter++;
				ProcessCollision(i, j);
			}
		}

	}
}

void SceneMgr::ProcessCollision(int i, int j)
{
	Object * obj1 = m_pObjects[i];
	Object * obj2 = m_pObjects[j];
	int kindObj1 = obj1->GetKind();
	int kindObj2 = obj2->GetKind();

	if (kindObj1 == KIND_BULLET && kindObj2 == KIND_BUILDING)
	{
		int bulletHP = obj1->GetHp();
		int buildingHP = obj2->GetHp();
		int resultBuildingHP = buildingHP - bulletHP;
		int resultBulletHP = 0;

		obj1->SetHp(resultBulletHP);
		obj2->SetHp(resultBuildingHP);
	}

	if (kindObj2 == KIND_BULLET && kindObj1 == KIND_BUILDING)
	{
		int bulletHP = obj2->GetHp();
		int buildingHP = obj1->GetHp();
		int resultBuildingHP = buildingHP - bulletHP;
		int resultBulletHP = 0;
		obj2->SetHp(resultBulletHP);
		obj1->SetHp(resultBuildingHP);
	}

	if (kindObj1 == KIND_BUILDING && kindObj2 == KIND_HERO)
	{
		obj2->SetVel(Vec3D());
	}

	if (kindObj2 == KIND_BUILDING && kindObj1 == KIND_HERO)
	{
		obj1->SetVel(Vec3D());
	}

	if (kindObj1 == KIND_HERO && kindObj2 == KIND_ENEMY)
	{
		//float m1, m2;
		//Vec3D ob1Vel, ob2Vel;
		//float rVx1, rVy1, rVz1, rVx2, rVy2, rVz2;
		//m1 = obj1->GetMass();
		//m2 = obj2->GetMass();
		//ob1Vel = obj1->GetVel();
		//ob2Vel = obj2->GetVel();
		//rVx1 = ((m1 - m2) / (m1 + m2)) * vx1 + (2.f * m2) / (m1 + m2) * vx2;
		//rVy1 = ((m1 - m2) / (m1 + m2)) * vy1 + (2.f * m2) / (m1 + m2) * vy2;
		//rVz1 = ((m1 - m2) / (m1 + m2)) * vz1 + (2.f * m2) / (m1 + m2) * vz2;
		//
		//rVx2 = ((2.f * m1) / (m1 + m2)) * vx1 + ((m2 - m1) / (m1 + m2)) * vx2;
		//rVy2 = ((2.f * m1) / (m1 + m2)) * vy1 + ((m2 - m1) / (m1 + m2)) * vy2;
		//rVz2 = ((2.f * m1) / (m1 + m2)) * vz1 + ((m2 - m1) / (m1 + m2)) * vz2;
		//
		//obj1->SetVelocity(rVx1, rVy1, rVz1);
		//obj2->SetVelocity(rVx2, rVy2, rVz2);
	}
}

bool SceneMgr::RRCollision(float minX, float minY, float maxX, float maxY
	, float minX1, float minY1, float maxX1, float maxY1)
{
	if (minX > maxX1) return false;
	if (maxX < minX1) return false;
	if (minY > maxY1) return false;
	if (maxY < minY1) return false;

	return true;
}

void SceneMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_pRenderer->DrawTextureRect(Vec3D(), Vec3D(800.f,600.f,0.f), Color(), m_BackgroundTexture);

	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		if (m_pObjects[i] != nullptr)
		{
			switch (m_pObjects[i]->GetKind())
			{
			case KIND_HERO:
				m_pRenderer->DrawTextureRectHeight(m_pObjects[i]->GetPos()*100.f, m_pObjects[i]->GetSize()*100.f, m_pObjects[i]->GetColor(), m_TestTexture);
				break;
			case KIND_BULLET:
				m_pRenderer->DrawTextureRectHeight(m_pObjects[i]->GetPos()*100.f, m_pObjects[i]->GetSize()*100.f, m_pObjects[i]->GetColor(), m_BulletTexture);
				break;
			case KIND_ENEMY:
				m_pRenderer->DrawTextureRectSeqXY(m_pObjects[i]->GetPos()*100.f, m_pObjects[i]->GetSize()*100.f, m_pObjects[i]->GetColor(), m_EnemyTexture, m_IdxX, m_IdxY, 4, 4);
				break;
			case KIND_BUILDING:
				m_pRenderer->DrawTextureRectSeqXY(m_pObjects[i]->GetPos()*100.f, m_pObjects[i]->GetSize()*100.f, m_pObjects[i]->GetColor(), m_AniTexture, m_IdxX, m_IdxY, 5, 8);
				break;
			}
		}
	}
}

void SceneMgr::Shoot(int _iDir)
{
	if (_iDir == SHOOT_NONE) return;
	if (!m_pObjects[HERO_ID]->CanFireBullet()) return;
	float bvX = 0.f, bvY = 0.f, amount = 10.f;

	switch (_iDir)
	{
	case SHOOT_UP:
		bvX = 0.f;
		bvY = amount;
		break;
	case SHOOT_DOWN:
		bvX = 0.f;
		bvY = -amount;
		break;
	case SHOOT_RIGHT:
		bvX = amount;
		bvY = 0.f;
		break;
	case SHOOT_LEFT:
		bvX = -amount;
		bvY = 0.f;
		break;
	}
	Vec3D pos = m_pObjects[HERO_ID]->GetPos();
	Vec3D vel = m_pObjects[HERO_ID]->GetVel();
	bvX = bvX + vel.x;
	bvY = bvY + vel.y;
	AddObject(pos.x, pos.y, 0.5f, bvX, bvY, 0.0f, 0.144f, 0.158f, 2.0f, KIND_BULLET, 1, STATE_GROUND);
	m_pObjects[HERO_ID]->InitBulletCoolTime();
}

void SceneMgr::DoGarbageCollect()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_pObjects[i] == NULL) continue;
		//Hp 가 0이면 죽인다
		if (m_pObjects[i]->GetHp() == 0) {
			DeleteObject(i);
			continue;
		}
		//화면 밖을 나가는 총알 죽인다
		Vec3D Pos = m_pObjects[i]->GetPos();
		float WidthMeter = m_ScreenWidth / 100.f;
		float HeightMter = m_ScreenHeight / 100.f;
		if (Pos.x > WidthMeter * 0.5f || Pos.x < -WidthMeter * 0.5f || Pos.y > HeightMter * 0.5f || Pos.y < -HeightMter * 0.5f)
		{
			if (m_pObjects[i]->GetKind() == KIND_BULLET) {
				DeleteObject(i);
				continue;
			}
		}
	}
}