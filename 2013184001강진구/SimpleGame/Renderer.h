#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include "Vector.h"
#include "Color.h"
#include "Dependencies\glew.h"
class Renderer
{
public:
	Renderer(int windowSizeX, int windowSizeY);
	~Renderer();
	
	bool IsInitialized();
	void DrawSolidRect(const Vec3D& pos,const Vec3D& size, const Color& color);
	void DrawTextureRect(const Vec3D& pos, const Vec3D& size, const Color& color, GLuint texID);
	void DrawTextureRectHeight(const Vec3D& pos, const Vec3D& size, const Color& color, GLuint texID);
	void DrawTextureRectSeqXY(const Vec3D& pos, const Vec3D& size, const Color& color, GLuint texID, int currSeqX, int currSeqY, int totalSeqX, int totalSeqY);
	GLuint CreatePngTexture(char * filePath);
	void DeleteTexture(GLuint texID);

private:
	void Initialize(int windowSizeX, int windowSizeY);
	bool ReadFile(char* filename, std::string *target);
	void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
	GLuint CompileShaders(char* filenameVS, char* filenameFS);
	void CreateVertexBufferObjects();
	void GetGLPosition(float x, float y, float *newX, float *newY);
	void GetGLShadow(float x, float *newV);

	bool m_Initialized = false;
	
	unsigned int m_WindowSizeX = 0;
	unsigned int m_WindowSizeY = 0;

	GLuint m_TexShadow = 0;
	GLuint m_VBORect = 0;
	GLuint m_SolidRectShader = 0;
	GLuint m_VBOTexRect = 0;
	GLuint m_TextureRectShader = 0;
	GLuint m_TextureRectSeqShader = 0;
};

