#pragma once
#include "Renderer.h"
#include "Global.h"
#include "Object.h"
class SceneMgr
{
public:
	SceneMgr(int _iWidth, int _iHeight);
	~SceneMgr();

public:
	void Update(float _fTime);
	void ApplyForce(Vec3D& force,float _fTime);
	void UpdateCollision();	
	void RenderScene();
	void Shoot(int _iDir);
	void DoGarbageCollect();

private:
	void Initialize(int _iWidth, int _iHeight);
	void DeleteObject(unsigned int _Idx);
	int	AddObject(float _fPosX, float _fPosY, float _fPosZ, float _fVelX, float _fVelY, float _fVelZ,
		float _fWidth, float _fHeight, float _fDepth, int _iKind, int _iHp, int _iState);
	int FindEmptySlot();
	void ProcessCollision(int i, int j);
	bool RRCollision(float minX, float minY, float maxX, float maxY,
		float minX1, float minY1, float maxX1, float maxY1);

private:
	float m_DeletaTime;
	Renderer* m_pRenderer;
	Object* m_pObjects[MAX_OBJECTS];
	GLuint m_TestTexture;
	GLuint m_AniTexture;
	GLuint m_BulletTexture;
	GLuint m_EnemyTexture;
	GLuint m_BackgroundTexture;
	int m_IdxX;
	int m_IdxY;
	int m_ScreenWidth;
	int m_ScreenHeight;
};

