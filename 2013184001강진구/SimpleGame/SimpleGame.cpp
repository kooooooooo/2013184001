/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <Windows.h>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "SceneMgr.h"

const int g_ScreenWidth = 800;
const int g_ScreenHeight = 600;

SceneMgr* g_SceneMgr = NULL;
DWORD g_PrevRenderTime = (unsigned long)0;

bool g_KeyW = false;
bool g_KeyA = false;
bool g_KeyS = false;
bool g_KeyD = false;
bool g_KeySP = false;

int g_Shoot = 0;

void RenderScene(void)
{	
	if (g_PrevRenderTime == 0)
	{
		g_PrevRenderTime = timeGetTime();
	}

	//Elapsed Time
	DWORD currentTime = timeGetTime();
	DWORD elapseTime = currentTime - g_PrevRenderTime;
	g_PrevRenderTime = currentTime;
	float eTime = (float)elapseTime / 1000.f; //convert to second

	//float ForceX = 0.0f, ForceY = 0.0f, ForceZ = 0.0f;
	Vec3D Force;
	float Amount = 2.5f;
	float amountZ = 200.f;
	if (g_KeyW) { Force.y += Amount;}
	if (g_KeyA) { Force.x -= Amount;}
	if (g_KeyS) { Force.y -= Amount;}
	if (g_KeyD) { Force.x += Amount;}
	if (g_KeySP) { printf("adsd\n"); Force.z += amountZ; }
	
	g_SceneMgr->ApplyForce(Force, eTime);
	g_SceneMgr->Update(eTime);
	g_SceneMgr->Shoot(g_Shoot);
	g_SceneMgr->UpdateCollision();
	g_SceneMgr->DoGarbageCollect();
	g_SceneMgr->RenderScene();

	glutSwapBuffers();

	//std::cout << "W: " << g_KeyW << "\tA: " << g_KeyA << "\tS: " << g_KeyS << "\tD: " << g_KeyD << std::endl;
	//std::cout << "SP: " << g_KeySP << std::endl;
}

void Idle(void)
{	
	RenderScene();
}

void MouseInput(int button, int state, int x, int y)
{
	RenderScene();
}

void KeyDownInput(unsigned char key, int x, int y)
{
	float amount = 2.0f;

	if (key == 'w')
	{
		g_KeyW = true;	
	}
	if (key == 'a')
	{
		g_KeyA = true;
	}
	if (key == 's')
	{
		g_KeyS = true;
	}
	if (key == 'd')
	{
		g_KeyD = true;
	}
	if (key == ' ')
	{
		g_KeySP = true;
	}

	RenderScene();
}

void KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w')
	{
		g_KeyW = false;		
	}
	if (key == 'a')
	{
		g_KeyA = false;		
	}
	if (key == 's')
	{
		g_KeyS = false;		

	}
	if (key == 'd')
	{
		g_KeyD = false;		
	}
	if (key == ' ')
	{
		g_KeySP = false;
	}

	RenderScene();
}

void SpecialKeyDownInput(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		g_Shoot = SHOOT_UP;

		break;
	case GLUT_KEY_DOWN:
		g_Shoot = SHOOT_DOWN;

		break;
	case GLUT_KEY_RIGHT:
		g_Shoot = SHOOT_RIGHT;
	
		break;
	case GLUT_KEY_LEFT:
		g_Shoot = SHOOT_LEFT;
		break;
	}

	RenderScene();
}

void SpecialKeyUpInput(int key, int x, int y)
{
	g_Shoot = SHOOT_NONE;

	RenderScene();
}

int main(int argc, char **argv)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetBreakAlloc(823);

	/*
	glut 관련 함수들은 가급적
	SimpleGame.cpp에 두는게 좋다
	*/

	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(g_ScreenWidth, g_ScreenHeight);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0")){
		std::cout << " GLEW Version is 3.0\n ";
	}
	else{
		std::cout << "GLEW 3.0 not supported\n ";
	}	

	// repeat off
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	//  glut 초기화 이후 SceneMgr 생성
	g_SceneMgr = new SceneMgr(g_ScreenWidth, g_ScreenHeight);

	glutMainLoop();

	// Release
	delete g_SceneMgr;

    return 0;
}

