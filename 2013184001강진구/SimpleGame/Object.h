#pragma once
#include "Vector.h"
#include "Color.h"
class Object
{
public:
	Object();
	~Object();
public:
	// 교수님 Get 함수 방식이 나랑 좀 다르네
	inline Vec3D& GetPos() { return m_fPos; };
	inline void SetPos(const Vec3D & other) { m_fPos = other; }
	inline void SetPosX(float x) { m_fSize.x = x; }
	inline void SetPosY(float y) { m_fSize.y = y; }
	inline void SetPosZ(float z) { m_fSize.z = z; }

	inline Vec3D& GetSize() { return m_fSize; }
	inline void SetSize(const Vec3D& other) { m_fSize = other; }
	inline void SetWidth(float width) { m_fSize.x = width; }
	inline void SetHeight(float height) { m_fSize.y = height; }
	inline void SetDepth(float depth) { m_fSize.z = depth; }

	inline Vec3D& GetVel() { return m_fVel; }
	inline void SetVel(const Vec3D& other) { m_fVel = other; }
	inline void SetVelX(float velx) { m_fVel.x = velx; }
	inline void SetVelY(float vely) { m_fVel.y = vely; }
	inline void SetVelZ(float velz) { m_fVel.z = velz; }

	inline Vec3D& GetAcc() { return m_fAcc; }
	inline void SetAcc(const Vec3D& other) { m_fAcc = other; }
	inline void SetAccX(float accx) { m_fAcc.x = accx; }
	inline void SetAccY(float accy) { m_fAcc.y = accy; }
	inline void SetAccZ(float accz) { m_fAcc.z = accz; }

	inline Color& GetColor() { return m_fColor; }
	inline void SetColor(const Color& color) { m_fColor = color; }

	inline float GetMass() { return m_fMass; }
	inline void SetMass(float f) { m_fMass = f; }

	inline float GetFrictionCoef() { return m_fFrictionCoef; }
	inline void SetFrictionCoef(float f) { m_fFrictionCoef = f; }

	inline int GetKind() { return m_iKind; }
	inline void SetKind(int k) { m_iKind = k; }

	inline int GetHp() { return m_iHp; }
	inline void SetHp(int hp) { m_iHp = hp; }

	inline int GetState() { return m_iState; }
	inline void SetState(int state) { m_iState = state; }

public:
	void InitBulletCoolTime();
	bool CanFireBullet();
	void Update(float _fTime);
	void ApplyForce(Vec3D& force, float _fTime);

private:
	Vec3D m_fPos;  // x,y,z
	Vec3D m_fVel;  // velx,vely,velz;
	Vec3D m_fAcc;  // accx,accy,accz;
	Vec3D m_fSize; // width,height,depth
	Color m_fColor;
	float m_fMass;
	float m_fFrictionCoef;
	int m_iKind;
	int m_iHp;
	int m_iState;
	float m_fBulletCoolTime;
	float m_fBulletTime;
};
