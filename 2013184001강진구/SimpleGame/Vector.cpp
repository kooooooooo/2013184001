#include "stdafx.h"
#include "Vector.h"


Vec3D::Vec3D() :x(0.0f), y(0.0f), z(0.0f)
{
}

Vec3D::Vec3D(float _x, float _y, float _z) : x(_x), y(_y), z(_z)
{
}

Vec3D::Vec3D(const Vec3D & other) : x(other.x), y(other.y), z(other.z)
{
}

Vec3D::~Vec3D()
{
}



