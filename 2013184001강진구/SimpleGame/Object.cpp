#include "stdafx.h"
#include "Object.h"
#include "Global.h"

#define MAX_VELOCITY 2.5f

Object::Object() :m_fBulletCoolTime(0.0f),m_fBulletTime(0.2f)
{
}

Object::~Object()
{
}

void Object::InitBulletCoolTime()
{
	m_fBulletCoolTime = m_fBulletTime;
}

bool Object::CanFireBullet()
{
	if (m_fBulletCoolTime <= 0.f){
		return true;
	}
	return false;
}

void Object::Update(float _fTime)
{
	// process cooltime
	m_fBulletCoolTime -= _fTime;
	if (m_fBulletCoolTime <= 0.f){ m_fBulletCoolTime = -1.f;}

	// calc friction
	/*마찰 계산*/
	float gz = m_fMass * GRAVITY;
	float friction = m_fFrictionCoef * gz;
	float velMag = m_fVel.Magnitude();
	
	//float velMag = sqrtf(m_fVelX * m_fVelX + m_fVelY * m_fVelY + m_fVelZ * m_fVelZ);
	// 속도의 크기가 0일 경우 아무것도 하지 않고
	
	if (velMag < FLT_EPSILON)
	{
		m_fVel.x = 0.0f;
		m_fVel.y = 0.0f;
		m_fVel.z = 0.0f;
	}
	else
	{
		float fx = -friction * m_fVel.x / velMag;
		float fy = -friction * m_fVel.y / velMag;
		float accX = fx / m_fMass;
		float accY = fy / m_fMass; 
		float newVelX = m_fVel.x + accX * _fTime;
		float newVelY = m_fVel.y + accY * _fTime;

		if (newVelX * m_fVel.x < 0.0f){
			m_fVel.x = 0.0f;
		}
		else{
			m_fVel.x = newVelX;
			if (MAX_VELOCITY <= m_fVel.x){
				m_fVel.x = MAX_VELOCITY;
			}
			else if (-MAX_VELOCITY >= m_fVel.x){
				m_fVel.x = -MAX_VELOCITY;
			}
		}

		if (newVelY * m_fVel.y < 0.0f)
		{
			m_fVel.y = 0.0f;
		}
		else
		{
			m_fVel.y = newVelY;
			if (MAX_VELOCITY <= m_fVel.y)
			{
				m_fVel.y = MAX_VELOCITY;
			}
			else if (-MAX_VELOCITY >= m_fVel.y)
			{
				m_fVel.y = -MAX_VELOCITY;
			}
		}

		m_fVel.z = m_fVel.z - GRAVITY * _fTime;
	}

	m_fVel.x = m_fVel.x + m_fAcc.x * _fTime;
	m_fVel.y = m_fVel.y + m_fAcc.y * _fTime;
	m_fVel.z = m_fVel.z + m_fAcc.z * _fTime;
	
	m_fPos.x = m_fPos.x + m_fVel.x * _fTime;
	m_fPos.y = m_fPos.y + m_fVel.y * _fTime;
	m_fPos.z = m_fPos.z + m_fVel.z * _fTime;

	if (m_fPos.z > 0.f)
	{
  		m_iState = STATE_AIR;
	}
	else
	{
		m_iState = STATE_GROUND;
		m_fPos.z = 0.f;
		m_fVel.z = 0.f;
	}
}

void Object::ApplyForce(Vec3D & force, float _fTime)
{
	//Calc Acc
	m_fAcc = force / m_fMass;
	//Calc Vel
	m_fVel += m_fAcc * _fTime;
	
	m_fAcc.x = 0;
	m_fAcc.y = 0;
	m_fAcc.z = 0;
}
