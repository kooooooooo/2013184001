#pragma once

//#include <math.h>
#include <assert.h>
class Vec3D
{
public: /*Ctor & Dtor*/
	Vec3D();
	Vec3D(float _x, float _y, float _z);
	Vec3D(const Vec3D& other);
	~Vec3D();

public: /*Operator*/
	inline Vec3D operator+(const Vec3D& other){ return Vec3D(x + other.x, y + other.y, z + other.z); }
	inline Vec3D operator-(const Vec3D& other) { return Vec3D(x - other.x, y - other.y, z - other.z); }
	inline Vec3D operator* (float val) { return Vec3D(x*val, y*val, z*val); }
	inline Vec3D operator/ (float val) { assert(val != 0); return Vec3D(x / val, y / val, z / val);}
	inline Vec3D operator=(const Vec3D& other) {
		x = other.x;
		y = other.y;
		z = other.z;
		return *this;
	}

	inline Vec3D& operator+=(const Vec3D& other) {
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	}
	inline Vec3D& operator-=(const Vec3D& other) {
		x -= other.x;
		y -= other.y;
		z -= other.z;
		return *this;
	}
	inline Vec3D& operator*=(float val) {
		x *= val;
		y *= val;
		z *= val;
		return *this;
	}
	inline Vec3D& operator/=(float val) {
		x /= val;
		y /= val;
		z /= val;
		return *this;
	}

public: /*util*/
	inline float Magnitude() { return sqrtf(Square()); }
	inline float Square() { return x * x + y * y + z * z; }
	inline float DotProduct(const Vec3D& other) { return x * other.x + y * other.y + z * other.z; }
	inline Vec3D CrossProduct(const Vec3D& other) {
		float ni = y * other.z - z * other.y;
		float nj = z * other.x - x * other.z;
		float nk = x * other.y - y * other.x;
		return Vec3D(ni, nj, nk);
	}
	inline Vec3D Normalization() {
		assert(Magnitude() != 0);
		*this /= Magnitude();
		return *this;
	}
public:
	float x, y, z;
};
